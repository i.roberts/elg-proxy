module gitlab.com/i.roberts/elg-proxy

go 1.15

require (
	github.com/armon/go-socks5 v0.0.0-20160902184237-e75332964ef5
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/net v0.0.0-20201216054612-986b41b23924 // indirect
)
