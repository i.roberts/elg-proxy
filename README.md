# SOCKS proxy for ELG debugging

This is a variation on https://github.com/serjs/socks5-server designed to allow proxying into a kubernetes cluster.  As well as the SOCKS proxy itself, this image includes a mini HTTP server that serves a `proxy.pac` instructing the browser to use ths SOCKS proxy only for `*.svc.cluster.local` domains.

## Usage

Run the proxy as a pod in your kubernetes cluster:

```
kubectl run $PODNAME --restart='Never' --namespace $NAMESPACE --image registry.gitlab.com/i.roberts/elg-proxy:latest
```

Port forward both the SOCKS and HTTP ports:

```
kubectl port-forward -n $NAMESPACE $PODNAME 9999:1080 8000:8000
```

Configure your firefox browser to retrieve the proxy configuration from `http://localhost:8000/proxy.pac`, and to resolve DNS names on the remote side (this is critical to make `svc.cluster.local` hostnames work).
