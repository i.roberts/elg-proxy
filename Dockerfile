FROM golang:1.15-alpine as builder

WORKDIR /go/src/gitlab.com/i.roberts/elg-proxy
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-s' -o ./elg-proxy

FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/gitlab.com/i.roberts/elg-proxy/elg-proxy /
ENTRYPOINT ["/elg-proxy"]
