package main

import (
	"log"
	"os"
        "fmt"
        "net/http"
	"github.com/armon/go-socks5"
	"github.com/caarlos0/env"
)

type params struct {
	Port          string `env:"PROXY_PORT" envDefault:"1080"`
        HttpPort      string `env:"HTTP_PORT" envDefault:"8000"`
        ForwardedPort string `env:"FORWARDED_PORT" envDefault:"9999"`
}

func main() {
	// Working with app params
	cfg := params{}
	err := env.Parse(&cfg)
	if err != nil {
		log.Printf("%+v\n", err)
	}

	//Initialize socks5 config
	socsk5conf := &socks5.Config{
		Logger: log.New(os.Stdout, "", log.LstdFlags),
	}

	server, err := socks5.New(socsk5conf)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Start listening proxy service on port %s\n", cfg.Port)
        go func() {
            if err := server.ListenAndServe("tcp", ":"+cfg.Port); err != nil {
                    log.Fatal(err)
            }
        }()

        log.Printf("Starting HTTP server for PAC file on port %s\n", cfg.HttpPort)
        http.HandleFunc("/proxy.pac", func(res http.ResponseWriter, req *http.Request) {
            res.Header().Set("Content-Type", "application/x-ns-proxy-autoconfig")
            fmt.Fprintf(res, `function FindProxyForURL(url, host) {
                  if(dnsDomainIs(host, ".svc.cluster.local")) {
                    return "SOCKS5 localhost:%s";
                  } else {
                    return "DIRECT";
                  }
                }`, cfg.ForwardedPort)
        })

        if err := http.ListenAndServe(":"+cfg.HttpPort, nil); err != nil {
            log.Fatal(err)
        }
}
